#!/bin/sh

# support a tarball as first argument
if [ "$1" != "" ]; then
    MENU="$1"
else
    MENU=menu-data-$(date +%Y%m%d).tar.gz
fi

UTILDIR=utils/
TOPDIR=./
TARGETDIR=./menu-data/
HERE=$PWD

# cleanup
rm -f $MENU
rm -f $TARGETDIR/*.desktop 
rm -f $TARGETDIR/icons/* 

# get and unpack
if [ ! -f $MENU ]; then
    wget -c http://rookery.canonical.com/~mvo/gnome-app-install/$MENU
fi
(cd $TOPDIR ; tar xzvf $HERE/$MENU )

# post-process
bzr add $TARGETDIR/*.desktop
bzr add $TARGETDIR/icons/*

# update the codec information based on the Packages file output
$UTILDIR/gst-add.py ./menu-data-codecs/*.desktop

