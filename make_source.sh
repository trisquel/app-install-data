#!/bin/sh
#
#    Copyright (C) 2008-2011  Rubén Rodríguez <ruben@trisquel.info>
#    Copyright (C) 2022       Luis Guzman <ark@switnet.org>
#    Copyright (C) 2020       Mason Hock <mason@masonhock.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

MIRROR=http://archive.trisquel.org/trisquel
TRISQUEL_CODENAME=$1
TRISQUEL_VERSION=$2

if [ "1$TRISQUEL_CODENAME" = "1" ] || [ "1$TRISQUEL_VERSION" = "1" ]; then
	echo 'Usage:  ./make_source.sh $TRISQUEL_CODENAME $TRISQUEL_VERSION'
	exit 1
fi

# setup local apt
LOCAL_APT=`mktemp -d`
trap "rm -rf ${LOCAL_APT}" 0 HUP INT QUIT ILL ABRT FPE SEGV PIPE TERM

mkdir -p ${LOCAL_APT}/var/lib/apt/partial
mkdir -p ${LOCAL_APT}/var/cache/apt/archives/partial
mkdir -p ${LOCAL_APT}/etc/
mkdir -p ${LOCAL_APT}/var/lib/dpkg
touch ${LOCAL_APT}/var/lib/dpkg/status
touch ${LOCAL_APT}/etc/trusted.gpg
#[ $UID = 0 ] && id _apt > /dev/null 2>&1 && chown _apt ${LOCAL_APT}  -R

cat << EOF > ${LOCAL_APT}/etc/apt.conf
Dir::State "${LOCAL_APT}/var/lib/apt";
Dir::State::status "${LOCAL_APT}/var/lib/dpkg/status";
Dir::Etc::SourceList "${LOCAL_APT}/etc/apt.sources.list";
Dir::Etc::SourceParts "";
Dir::Cache "${LOCAL_APT}/var/cache/apt";
pkgCacheGen::Essential "none";
Dir::Etc::Trusted "${LOCAL_APT}/etc/trusted.gpg";
Acquire::ForceIPv4 "true";
EOF

cat << EOF > $LOCAL_APT/etc/apt.sources.list
deb $MIRROR $TRISQUEL_CODENAME main
deb $MIRROR $TRISQUEL_CODENAME-updates main
deb $MIRROR $TRISQUEL_CODENAME-security main
deb-src $MIRROR $TRISQUEL_CODENAME main
deb-src $MIRROR $TRISQUEL_CODENAME-updates main
deb-src $MIRROR $TRISQUEL_CODENAME-security main
EOF
apt-key --keyring ${LOCAL_APT}/etc/trusted.gpg adv --keyserver keyserver.ubuntu.com --recv-keys B138CA450C05112F
apt-file update -c $LOCAL_APT/etc/apt.conf

# get list of desktop files
apt-file -c $LOCAL_APT/etc/apt.conf search "/usr/share/applications" | /bin/sed 's| /usr/share/applications/||' > /tmp/desktop_packages.list

# get list of packages
rm -f /tmp/packages.list
for package in $(cat /tmp/desktop_packages.list | sed 's/:.*//' | sort -u); do
  apt-cache -c $LOCAL_APT/etc/apt.conf showsrc $package | grep ^Binary: | sed 's/^Binary: //' | tr -d , | sed 's/ /\n/g' | sort -u >> /tmp/packages.list
done
echo hicolor-icon-theme >> /tmp/packages.list
echo gnome-icon-theme >> /tmp/packages.list
echo gnome-colors-common >> /tmp/packages.list

# create partial mirror
PARTIAL_MIRROR=$PWD/partial_mirror_$TRISQUEL_CODENAME
mkdir -p $PARTIAL_MIRROR/conf

cat << EOF > $PARTIAL_MIRROR/conf/updates
Name: $TRISQUEL_CODENAME
Method: http://archive.trisquel.org/trisquel
Suite: $TRISQUEL_CODENAME
Components: main>main
Architectures: amd64
VerifyRelease: B138CA450C05112F
FilterList: purge packages.list

Name: $TRISQUEL_CODENAME-updates
Method: http://archive.trisquel.org/trisquel
Suite: $TRISQUEL_CODENAME-updates
Components: main>main
Architectures: amd64
VerifyRelease: B138CA450C05112F
FilterList: purge packages.list

Name: $TRISQUEL_CODENAME-security
Method: http://archive.trisquel.org/trisquel
Suite: $TRISQUEL_CODENAME-security
Components: main>main
Architectures: amd64
VerifyRelease: B138CA450C05112F
FilterList: purge packages.list
EOF

cat << EOF > $PARTIAL_MIRROR/conf/distributions
Origin: Trisquel
Label: Trisquel
Suite: $TRISQUEL_CODENAME
Codename: $TRISQUEL_CODENAME
Architectures: amd64
Components: main
DebIndices: Packages Release . .gz .bz2
Update: $TRISQUEL_CODENAME $TRISQUEL_CODENAME-updates $TRISQUEL_CODENAME-security
EOF

# update mirror
cat /tmp/packages.list | sort -u | sed 's/$/ install/' > $PARTIAL_MIRROR/conf/packages.list
reprepro -Vb $PARTIAL_MIRROR --noskipold update $TRISQUEL_CODENAME

# extract desktop files and icons
mkdir -p extracted_files_$TRISQUEL_CODENAME
for package in $(cat /tmp/packages.list | sed 's/:.*//' | sort -u); do
    deb_path=$(find $PARTIAL_MIRROR/ | grep "${package}_.*.deb")
    (
	cd extracted_files_$TRISQUEL_CODENAME

	desktops=$(dpkg-deb --contents $deb_path | sed 's/.* //g' | grep '\.desktop$' | grep -v /$)
	for desktop in $desktops; do
            dpkg-deb --fsys-tarfile $deb_path | tar -xvf - --wildcards $desktop
        done

	icons=$(dpkg-deb --contents $deb_path | sed 's/.* //g' | grep '/hicolor/.*\.svg$')
	for icon in $icons; do
            dpkg-deb --fsys-tarfile $deb_path | tar -xvf - --wildcards $icon
        done

	icons=$(dpkg-deb --contents $deb_path | sed 's/.* //g' | grep '/gnome/.*\.svg$')
	for icon in $icons; do
            dpkg-deb --fsys-tarfile $deb_path | tar -xvf - --wildcards $icon
        done

	icons=$(dpkg-deb --contents $deb_path | sed 's/.* //g' | grep '/gnome-colors-common/.*\.svg$')
	for icon in $icons; do
            dpkg-deb --fsys-tarfile $deb_path | tar -xvf - --wildcards $icon
        done
    )
done


# download popcon data
curl -s https://popcon.debian.org/by_vote | grep ^[^#] | awk '{print$2,$4}' > /tmp/popcon_data

# repurpose debian data - names:
# clean debian firefox and abrowser packages in favor of default firefox-esr and finally replace as "firefox"
# clean debian icedove in favor of default "thunderbird"
sed -i "/^abrowser/d" /tmp/popcon_data
sed -i "/^firefox /d" /tmp/popcon_data
sed -i "/^icedove/d" /tmp/popcon_data
sed -i "/^firefox-l10n/d" /tmp/popcon_data
sed -i "/^firefox-esr/s|-esr||" /tmp/popcon_data
sed -i "/^firefox-l10n/s|l10n|locale|" /tmp/popcon_data
sed -i "/^thunderbird-l10n/s|l10n|locale|" /tmp/popcon_data
# add renamed packages to popcon_data
sed -i "/^firefox /a$(grep '^firefox ' /tmp/popcon_data | /bin/sed 's/firefox/abrowser/')" /tmp/popcon_data
sed -i "/^firefox /a$(grep '^firefox ' /tmp/popcon_data | /bin/sed 's/firefox/icecat/')" /tmp/popcon_data
sed -i "/^thunderbird /a$(grep '^thunderbird ' /tmp/popcon_data | /bin/sed 's/thunderbird/icedove/')" /tmp/popcon_data

# copy template to new source directory
rm -rf source_$TRISQUEL_CODENAME
cp -a source_template source_$TRISQUEL_CODENAME

# create changelog
(
    cd source_$TRISQUEL_CODENAME
    export DEBFULLNAME="Trisquel GNU/Linux developers"
    export DEBEMAIL=trisquel-devel@listas.trisquel.info
    dch \
        --create \
	--package app-install-data \
	--newversion 1:${TRISQUEL_VERSION}-$(date --utc +%Y%m%d) \
        --distribution $TRISQUEL_CODENAME \
	"Generate application data for $TRISQUEL_CODENAME"
)

# Manually add icedove
THUNDERBIRD_ICON='https://gitlab.trisquel.org/trisquel/package-helpers/-/raw/a6cd1ba0f90ab7c05be8feec62736c3e8cb48ff7/helpers/DATA/app-install-data-ubuntu/menu-data/icons/icedove.png?inline=false'
wget -4 $THUNDERBIRD_ICON -O source_$TRISQUEL_CODENAME/menu-data/icons/icedove.png
sed -i '/OnlyShowIn/d' extracted_files_$TRISQUEL_CODENAME/usr/share/applications/icedove.desktop
cp extracted_files_$TRISQUEL_CODENAME/usr/share/applications/icedove.desktop source_$TRISQUEL_CODENAME/menu-data/icedove\:icedove.desktop
#End manual changes

# create menu data
for line in $(cat /tmp/desktop_packages.list); do
    package=$(echo $line | /bin/sed 's/:.*//')
    desktop=$(echo $line | /bin/sed 's/.*://')
    if [ -f extracted_files_$TRISQUEL_CODENAME/usr/share/applications/$desktop ]; then
        popcon=1
	vote="$(grep "$package [0-9]" /tmp/popcon_data | head -n1 | awk '{print$2}')" || popcon=0
	if [ $popcon = 1 ]; then
            cp extracted_files_$TRISQUEL_CODENAME/usr/share/applications/$desktop source_$TRISQUEL_CODENAME/menu-data/$package:$(echo $desktop | /bin/sed 's|.*/||g')
            /bin/sed -i "/^.Desktop Entry.$/aX-AppInstall-Section=main" source_$TRISQUEL_CODENAME/menu-data/$package:$(echo $desktop | /bin/sed 's|.*/||g')
            /bin/sed -i "/^.Desktop Entry.$/aX-AppInstall-Popcon=${vote}" source_$TRISQUEL_CODENAME/menu-data/$package:$(echo $desktop | /bin/sed 's|.*/||g')
            /bin/sed -i "/^.Desktop Entry.$/aX-AppInstall-Package=${package}" source_$TRISQUEL_CODENAME/menu-data/$package:$(echo $desktop | /bin/sed 's|.*/||g')
        fi
    fi
done


# remove unwanted menu data
(
    cd source_$TRISQUEL_CODENAME/menu-data
    # only shown in some desktop environments
    rm -rf $(grep -lr OnlyShowIn . | grep '\.desktop$')
    rm -rf $(grep -lr NotShowIn . | grep '\.desktop$')
    # not shown in any desktop environments
    rm -rf $(grep -lr NoDisplay=true . | grep '\.desktop$')
    # no icon defined
    rm -rf $(grep -Lr Icon . | grep '\.desktop$')
    # runs in terminal
    rm -rf $(grep -lr "Terminal=true" ./* | grep '\.desktop$')
    # is a symlink
    rm -rf $(find -type l)
)

#Known problematic desktop files
[ $TRISQUEL_CODENAME = "aramo" ] && \
  rm source_$TRISQUEL_CODENAME/menu-data/camitk-actionstatemachine:camitk-actionstatemachine.desktop

# find icons
for desktop_file in source_$TRISQUEL_CODENAME/menu-data/*.desktop; do
    icon_name=$(grep ^Icon= $desktop_file | sed 's/^Icon=//' | sed '/\//d')
    if [ "1$icon_name" != "1" ]; then
        svgs=$(find extracted_files_$TRISQUEL_CODENAME -type f -iname $icon_name.svg)
        hicolor=$(echo $svgs | sed 's/ /\n/g' | grep '/hicolor/')
        gnome_colors_common=$(echo $svgs | sed 's/ /\n/g' | grep '/gnome-colors-common/')
        gnome=$(echo $svgs | sed 's/ /\n/g' | grep '/gnome/')
        fallback=$(echo $svgs | sed 's/ .*//g')
        icon_path=$(echo $hicolor $gnome_colors_common $gnome $fallback | sed 's/ .*//g')
        if [ "1$icon_path" != "1" ]; then
            cp $icon_path source_$TRISQUEL_CODENAME/menu-data/icons
        fi
    fi
done
